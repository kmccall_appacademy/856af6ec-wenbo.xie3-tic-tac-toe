class HumanPlayer
  attr_reader :name

  def initialize(name)
    @name = name.capitalize
  end

  def get_move
    print "Where do you want to move? "
    move = gets.chomp

    move.split(",").map(&:to_i)
  end

  def display(board)
    print board.grid
  end
end
