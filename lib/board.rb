class Board
  attr_reader :grid

  def initialize(grid=nil)
    @grid = grid || Array.new(3) { Array.new(3) }
  end

  def [](pos)
    x, y = pos
    grid[x][y]
  end

  def []=(pos, mark)
    x, y = pos
    grid[x][y] = mark
  end

  def place_mark(pos, mark)
    self[pos] = mark
  end

  def empty?(pos)
    self[pos].nil?
  end

  def winner
    solutions = solution_manual

    [:X, :O].each do |winner|
      if solutions.any? { |triplet| triplet.all? { |mark| winner == mark } }
        return winner
      end
    end

    nil
  end

  def over?
    winner || grid.none? { |triplet| triplet.all?(&:nil?) }
  end

  def solution_manual
    triplets = []
    triplet_x = []
    triplet_y = []
    left_diag = [self[[0, 0]], self[[1, 1]], self[[2, 2]]]
    right_diag = [self[[0, 2]], self[[1, 1]], self[[2, 0]]]
    solutions = [left_diag, right_diag]

    (0..2).each do |x|
      (0..2).each do |y|
        triplet_x << self[[x, y]]
        triplet_y << self[[y, x]]
      end

      triplets << triplet_x << triplet_y
      triplet_x, triplet_y = [[], []]
    end

    triplets.each do |triplet|
      solutions << triplet
    end

    solutions
  end
end
