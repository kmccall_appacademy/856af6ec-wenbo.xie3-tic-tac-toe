class ComputerPlayer
  attr_reader :name, :board
  attr_accessor :mark

  def initialize(name)
    @name = name
    @mark = :O
  end

  def display(board)
    @board = board
  end

  def get_move
    (0..2).each do |num1|
      (0..2).each do |num2|
        pos = [num1, num2]

        return pos if winning_move?(pos)
      end
    end

    random_move
  end

  def winning_move?(pos)
    return false unless board.empty?(pos)

    board[pos] = mark

    if board.winner == mark
      board[pos] = nil
      true
    else
      board[pos] = nil
      false
    end
  end

  def random_move
    random = [rand(2), rand(2)]

    board.empty?(random) ? random : random_move
  end
end
