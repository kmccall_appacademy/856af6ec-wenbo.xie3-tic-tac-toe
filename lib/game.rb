require_relative 'board'
require_relative 'human_player'
require_relative 'computer_player'

class Game
  attr_accessor :board, :current_player
  attr_reader :player1, :player2

  def initialize(player1, player2)
    @player1 = player1
    @player2 = player2
    player1.mark = :X
    player2.mark = :O
    @current_player = player1
    @board = Board.new
  end

  def play_turn
    move = current_player.get_move
    mark = current_player.mark

    board.place_mark(move, mark)
    switch_players!
  end

  def switch_players!
    if current_player == player1
      self.current_player = player2
    else
      self.current_player = player1
    end
  end
end
